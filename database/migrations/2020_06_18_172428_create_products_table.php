<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('sku', 50);
            $table->string('slug');
            $table->string('image', 50)->nullable();
            $table->text('thumbs')->nullable();
            $table->text('description')->nullable();
            $table->longText('content')->nullable();
            $table->string('type', 20);
            $table->integer('stock')->nullable();
            $table->float('price');
            $table->float('sale')->nullable();
            $table->string('status', 50)->default('publish');
            $table->integer('orderBy');
            $table->ipAddress('visitor');
            $table->text('metaTitle')->nullable();
            $table->text('metaDescription')->nullable();
            $table->text('metaKeyword')->nullable();
            $table->timestamps();

            $table->index('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
