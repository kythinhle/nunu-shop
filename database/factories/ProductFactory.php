<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Product;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;

define('imageWidth', 640);
define('imageHeight', 480);
define('imageCatalog', 'fashion');
define('productType', 'normal');

$factory->define(Product::class, function (Faker $faker) {
	$name = $faker->unique()->text( 20 );
	$thumbs = [];
	for($i = 0; $i <= 4; $i++) {
		array_push($thumbs, $faker->imageUrl(imageWidth, imageHeight, imageCatalog)) ;
	}
    return [
        'name' => $name,
        'sku'  => sprintf('SKU %d', $faker->randomDigitNotNull),
        'slug' => Str::slug($name),
        'image' => $faker->imageUrl(imageWidth, imageHeight, imageCatalog),
        'thumbs' => collect($thumbs)->implode(','),
        'description' => $faker->realText(200),
        'content' => $faker->realText(500),
        'type' => productType,
        'stock' => $faker->randomDigitNotNull,
        'price' => $faker->randomNumber(2),
        'sale' => 0,
        'orderBy' => $faker->randomDigitNotNull,
        'visitor' => $faker->ipv4,
        'metaTitle' => $faker->realText(200),
        'metaDescription' => $faker->realText(200),
        'metaKeyword' => $faker->realText(200)
    ];
});
